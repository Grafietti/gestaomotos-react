import React from 'react';
import Header from '../header';
import logo from '../logo.svg';
	
import {Link} from 'react-router-dom';

 
function Home(){  
  
    return (
      <div>        
        <Header title="Sistema de Gestão de Motocicletas" />
        <img src={logo} className="App-logo" alt="logo" /> <br/>
        <Link to="/incluir">Incluir</Link> <br/>
        <Link to="/selecionar">Selecionar</Link>
      </div>
    );
}
 
export default Home;