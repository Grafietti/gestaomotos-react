import React from 'react';
import {useState} from 'react';
import Header from '../header';
import axios from 'axios';
import {Link} from 'react-router-dom';

function Mensagem(props){     
    let mensagem = "";
    switch (props.status) {
        case 0: mensagem = 'Preencha os campos para novo cadastro.'; break;        
        case 1: mensagem = 'Cadastro executado com sucesso!'; break;
        case 2: mensagem = 'Erro ao efetuar o cadastro' + props.error; break;
        default: mensagem = "...em andamento";        
      }
    return <p> {mensagem} </p>
}
 
function FormIncluir(){
     
    const [campos, setCampos] = useState({        
        marca: '',
        modelo: ''
    });

    const[status, setStatus] = useState(0); //inicia em novo cadastro
    const[error, setError] = useState(null); //inicia sem qualquer erro
   
    function executaChange(event){
        campos[event.target.name] = event.target.value;
        setCampos(campos);
    }

    function executaSubmit(event){
        event.preventDefault();
        setStatus(9);
        console.log(campos);   
        axios.post('http://localhost:3030/motos/inserir', campos).then(response => {            
            setStatus(1); //Cadastro executado com sucesso!              
        }).catch(error => {
            setError(error);
            setStatus(2);            
        });    
    }

    function executaNovoCadastroClick(event){
        setStatus(0); //volta status inicial
        setError(null); //zero erros se houver       
        window.document.getElementById('marca').value=""; //limpa campos do form
        window.document.getElementById('modelo').value="";        
    }
 
    return (
        <div>
            <Header title="Cadastro de Motos" />
            <form onSubmit={executaSubmit}>
                <fieldset >                                            
                    <label>Marca:
                        <input type="text" name="marca" id="marca" onChange={executaChange} />
                    </label>
                    <label>Modelo:
                        <input type="text" name="modelo" id="modelo" onChange={executaChange} />
                    </label>
                    <input type="submit" value="Salvar" />                
                </fieldset>
            </form>
            <Mensagem status={status} error={error}/>
            <br></br>
                <input type="button" value="Novo Cadastro" onClick={executaNovoCadastroClick}/>
                <Link to="/selecionar"><input type="button" value="Selecionar"/></Link>                                         
        </div>        
    )
}
 
export default FormIncluir;