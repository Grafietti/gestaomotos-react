import React from 'react';
import {useEffect, useState} from 'react';
import Header from '../header';
import axios from 'axios';
import {Link} from 'react-router-dom';
 
function Selecionar(){

    const [motos, setMotos] = useState([]);
 
    useEffect(() => {
        axios.get('http://localhost:3030/motos/selecionar').then(response => {
            const lista = response.data;
            if (lista.length === 0) {
                return Promise.reject(new Error("Não ha registros para serem exibidos!"));
            }
            setMotos(lista);
        }).catch( error => {console.log(error)})
    }, []);


    return (
        <div>
            <Header title="Listagem de Motos" /> 
            <Link to="/incluir">Incluir</Link>           
            <table align="center" border="0">
                <tr> 
                    <th>id</th>   
                    <th>Marca</th>
                    <th>Modelo</th>
                </tr>            
                    { motos.map(moto => (
                        <tr key={moto.id}>
                            <td> {moto.id} </td>
                            <td> {moto.Marca} </td> 
                            <td> {moto.Modelo} </td>
                        </tr>
                    )) }
            </table>
            <p> -- fim de listagem -- </p>
        </div>        
    )
}
 
export default Selecionar;